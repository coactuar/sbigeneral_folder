<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SBI General Insurance :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="top-banner">
    <img src="img/top-banner.jpg" class="img-fluid" alt=""/> 
</div>

<div class="content-area">
  <!--<div id="logo">
    <img src="img/logo.png" class="logo"  alt=""/> 
  </div>-->
  <div id="loginForm">
    
    <form id="login-form" method="post" role="form">
      <div id="login-message"></div>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Name" name="empName" id="empName" autocomplete="off" required>
      </div>
      <div class="input-group">
        <input type="number" class="form-control" placeholder="Mobile Number" name="empid" id="empid" autocomplete="off" required>
      </div>
      <div class="input-group">
        <button id="login" class="login-button" type="submit">Login</button>
      </div>
    </form> 
  </div> 
</div>

      
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
     $('#login').attr('disabled', false);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      else if (data == '0')
      {
          $('#login-message').text('Invalid Employee Id. Please try again.');
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn();
      }
      $('#login').attr('disabled', false);
  });
  
  return false;
});
</script>
</body>
</html>